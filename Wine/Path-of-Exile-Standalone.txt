%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Creates isolated 64-bit prefix for Path of Exile
- Standalone

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Personal Notes
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Enable Gallium Nine
- Use Windowed Fullscreen and have a Virtual Desktop set to native resolution
- Use Generic Software on Pulseaudio
- Disable Shadows

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Install
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Install
%%%%%%%%%%%%%%%%%%%%

mkdir -p ~/'Wine Prefixes' && WINEPREFIX=~/'Wine Prefixes/Path of Exile' WINEARCH=win64 wineboot && WINEPREFIX=~/'Wine Prefixes/Path of Exile' winetricks 'grabfullscreen=y' 'sandbox' && wget 'https://www.pathofexile.com/downloads/PathOfExileInstaller.exe' -O ~/'Wine Prefixes/Path of Exile/drive_c/users/'$USER/'Downloads/PathOfExileInstaller.exe' && WINEPREFIX=~/'Wine Prefixes/Path of Exile' wine ~/'Wine Prefixes/Path of Exile/drive_c/users/'$USER/'Downloads/PathOfExileInstaller.exe'

%%%%%%%%%%%%%%%%%%%%
%%%%% Finish Up
%%%%%%%%%%%%%%%%%%%%

rm ~/'Wine Prefixes/Path of Exile/drive_c/users/'$USER/'Downloads/PathOfExileInstaller.exe' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Configuration
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Virtual Desktop (Desktop)
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Path of Exile' winetricks 'vd=1600x900'

%%%%%%%%%%%%%%%%%%%%
%%%%% Virtual Desktop (Laptop)
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Path of Exile' winetricks 'vd=1366x768'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Launcher
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Path of Exile
%%%%%%%%%%%%%%%%%%%%

-------------------------
rm -Rf ~/'.local/share/applications/wine/Programs/Grinding Gear Games' && mkdir -p ~/'.local/share/applications/wine/Programs/Path of Exile' && bash -c 'cat >  ~/".local/share/applications/wine/Programs/Path of Exile/Path of Exile.desktop"' << EOF
[Desktop Entry]
Name=Path of Exile
Categories=Game;
Exec=env WINEDEBUG=-all WINEPREFIX='/home/$USER/Wine Prefixes/Path of Exile' wine '/home/$USER/Wine Prefixes/Path of Exile/drive_c/Program Files (x86)/Grinding Gear Games/Path of Exile/PathOfExile.exe'
Type=Application
StartupNotify=true
Path=/home/$USER/Wine Prefixes/Path of Exile/drive_c/Program Files (x86)/Grinding Gear Games/Path of Exile
Icon=7F60_ClientIcon.0
EOF
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Quick Commands
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Winecfg
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Path of Exile' winecfg

%%%%%%%%%%%%%%%%%%%%
%%%%% Winetricks
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Path of Exile' winetricks

%%%%%%%%%%%%%%%%%%%%
%%%%% Regedit
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Path of Exile' regedit

%%%%%%%%%%%%%%%%%%%%
%%%%% Kill
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Path of Exile' wineserver -k

%%%%%%%%%%%%%%%%%%%%
%%%%% File Manager
%%%%%%%%%%%%%%%%%%%%

xdg-open ~/'Wine Prefixes/Path of Exile/drive_c/Program Files (x86)/Grinding Gear Games/Path of Exile'

%%%%%%%%%%%%%%%%%%%%
%%%%% Normal Execute
%%%%%%%%%%%%%%%%%%%%

cd ~/'Wine Prefixes/Path of Exile/drive_c/Program Files (x86)/Grinding Gear Games/Path of Exile' && WINEPREFIX=~/'Wine Prefixes/Path of Exile' wine ~/'Wine Prefixes/Path of Exile/drive_c/Program Files (x86)/Grinding Gear Games/Path of Exile/PathOfExile.exe'

%%%%%%%%%%%%%%%%%%%%
%%%%% Performance Graphs
%%%%%%%%%%%%%%%%%%%%

cd ~/'Wine Prefixes/Path of Exile/drive_c/Program Files (x86)/Grinding Gear Games/Path of Exile' && vblank_mode=0 GALLIUM_HUD='fps,GPU-load,draw-calls;cpu,cpu0+cpu1+cpu2+cpu3,cpu4+cpu5+cpu6+cpu7;VRAM-usage,requested-VRAM,requested-GTT' WINEPREFIX=~/'Wine Prefixes/Path of Exile' wine ~/'Wine Prefixes/Path of Exile/drive_c/Program Files (x86)/Grinding Gear Games/Path of Exile/PathOfExile.exe'

%%%%%%%%%%%%%%%%%%%%
%%%%% Content.ggpk Check
%%%%%%%%%%%%%%%%%%%%

cd ~/'Wine Prefixes/Path of Exile/drive_c/Program Files (x86)/Grinding Gear Games/Path of Exile' && WINEPREFIX=~/'Wine Prefixes/Path of Exile' wine ~/'Wine Prefixes/Path of Exile/drive_c/Program Files (x86)/Grinding Gear Games/Path of Exile/PackCheck.exe' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Remove Client.txt
%%%%%%%%%%%%%%%%%%%%

rm ~/'Wine Prefixes/Path of Exile/drive_c/Program Files (x86)/Grinding Gear Games/Path of Exile/logs/Client.txt' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Remove ShaderCache
%%%%%%%%%%%%%%%%%%%%

rm -R ~/'Wine Prefixes/Path of Exile/drive_c/Program Files (x86)/Grinding Gear Games/Path of Exile/ShaderCache' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Restore Content.ggpk
%%%%%%%%%%%%%%%%%%%%

mv ~/'Downloads/Path of Exile/Content.ggpk' ~/'Wine Prefixes/Path of Exile/drive_c/Program Files (x86)/Grinding Gear Games/Path of Exile' && rm -r ~/'Downloads/Path of Exile' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Rules
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Item Conditions
%%%%%%%%%%%%%%%%%%%%

- Cannot loot any normal-quality item (white) except the following:
- Must loot Rings and Amulets of any quality
- Must loot Flasks of any quality
- Must loot Gems and Jewels of any quality
- Must loot all Quest items at convenience
- Loot all currency and scrolls
- Loot all items Magic (blue) and higher
- Only identify items that are potential upgrades or new pieces or the following:
- Identify all flasks
- Cannot purchase items from vendors except the following:
- Can purchase items from Perandus vendor
- Can purchase scrolls (town portal and identity)
- Cannot trade with any other players, ever
- Cannot play with any other players except during PvP scenarios

%%%%%%%%%%%%%%%%%%%%
%%%%% Maps Conditions
%%%%%%%%%%%%%%%%%%%%

- Must explore every map encountered
- Must leave the map after 0 enemies remain and all item loot conditions are met
- Must make an attempt at breaking all breakables and opening all Chests
- In event of glitched or unlucky map (more than 0 enemies remaining after throughout search), map must be redone in new instance
- All quests (optional and mandatory) must be completed

%%%%%%%%%%%%%%%%%%%%
%%%%% Honour Conditions
%%%%%%%%%%%%%%%%%%%%

- Must never use Alt + F4, disconnect packet, or any other like-means of survival
- Must never try to log out or change characters in-event of near death on the field
- Can freely create Town Portals at boss locations and use at will

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################
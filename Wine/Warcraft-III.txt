%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Creates isolated 32-bit prefix for Warcraft III
- PvPGN Loader uses map editor icon

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Installation
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Reign of Chaos
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Warcraft III' WINEARCH=win32 wine ~/'Downloads/Warcraft III/Warcraft III 1.21b ROC Installer enUS/Installer.exe'

%%%%%%%%%%%%%%%%%%%%
%%%%% The Frozen Throne
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Warcraft III' wine ~/'Downloads/Warcraft III/WarCraft III 1.21b TFT Installer enUS/Installer.exe'

%%%%%%%%%%%%%%%%%%%%
%%%%% 1.26a Patch
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Warcraft III' wine ~/'Downloads/Warcraft III/War3TFT_126a_English.exe'

%%%%%%%%%%%%%%%%%%%%
%%%%% PvPGN Loader
%%%%%%%%%%%%%%%%%%%%

unzip ~/'Downloads/Warcraft III/w3l-1.26a.zip' -d ~/'Wine Prefixes/Warcraft III/drive_c/Program Files/Warcraft III'

%%%%%%%%%%%%%%%%%%%%
%%%%% Custom Maps
%%%%%%%%%%%%%%%%%%%%

mv ~/'Downloads/Warcraft III/Custom' ~/'Wine Prefixes/Warcraft III/drive_c/Program Files/Warcraft III/Maps'

%%%%%%%%%%%%%%%%%%%%
%%%%% Finish Up
%%%%%%%%%%%%%%%%%%%%

rm -R ~/'Downloads/Warcraft III' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Launcher
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Remove Old
%%%%%%%%%%%%%%%%%%%%

rm -R ~/'.local/share/applications/wine/Programs/Warcraft III' && mkdir -p ~/'.local/share/applications/wine/Programs/Warcraft III'

%%%%%%%%%%%%%%%%%%%%
%%%%% Reign of Chaos
%%%%%%%%%%%%%%%%%%%%

nano ~/'.local/share/applications/wine/Programs/Warcraft III/Warcraft III: Reign of Chaos.desktop'

-------------------------
[Desktop Entry]
Name=Warcraft III: Reign of Chaos
Categories=Game;
Exec=env WINEDEBUG=-all WINEPREFIX='/home/espionage724/Wine Prefixes/Warcraft III' wine '/home/espionage724/Wine Prefixes/Warcraft III/drive_c/Program Files/Warcraft III/Warcraft III.exe'
Type=Application
StartupNotify=true
Path=/home/espionage724/Wine Prefixes/Warcraft III/drive_c/Program Files/Warcraft III
Icon=938C_Warcraft III.0
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% The Frozen Throne
%%%%%%%%%%%%%%%%%%%%

nano ~/'.local/share/applications/wine/Programs/Warcraft III/Warcraft III: The Frozen Throne.desktop'

-------------------------
[Desktop Entry]
Name=Warcraft III: The Frozen Throne
Categories=Game;
Exec=env WINEDEBUG=-all WINEPREFIX='/home/espionage724/Wine Prefixes/Warcraft III' wine '/home/espionage724/Wine Prefixes/Warcraft III/drive_c/Program Files/Warcraft III/Frozen Throne.exe'
Type=Application
StartupNotify=true
Path=/home/espionage724/Wine Prefixes/Warcraft III/drive_c/Program Files/Warcraft III
Icon=DB92_Frozen Throne.0
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% PvPGN Loader
%%%%%%%%%%%%%%%%%%%%

nano ~/'.local/share/applications/wine/Programs/Warcraft III/Warcraft III: The Frozen Throne (PvPGN).desktop'

-------------------------
[Desktop Entry]
Name=Warcraft III: The Frozen Throne (PvPGN)
Categories=Game;
Exec=env WINEDEBUG=-all WINEPREFIX='/home/espionage724/Wine Prefixes/Warcraft III' wine '/home/espionage724/Wine Prefixes/Warcraft III/drive_c/Program Files/Warcraft III/w3l.exe'
Type=Application
StartupNotify=true
Path=/home/espionage724/Wine Prefixes/Warcraft III/drive_c/Program Files/Warcraft III
Icon=AD78_World Editor.0
-------------------------

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- DokuWiki
- Realm of Espionage
- Fedora Server 23

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Software
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo dnf install 'git-core'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Download Source
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cd '/var/www' && sudo git clone -b 'master' 'https://github.com/splitbrain/dokuwiki.git' '/var/www/wiki' && sudo chcon -R -t 'httpd_sys_rw_content_t' '/var/www/wiki' && sudo chown -R 'nginx':'nginx' '/var/www/wiki' && sudo chgrp -R 'nginx' '/var/lib/php/session' '/var/lib/php/wsdlcache' && cd ~ && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% nginx Server Block
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/nginx/conf.d/wiki-site.conf' && sudo systemctl reload 'nginx'

-------------------------
server {
    listen 443 ssl spdy;
    server_name wiki.realmofespionage.xyz;
    server_name_in_redirect off;
    root /var/www/wiki;
    index doku.php;

    include /etc/nginx/default.d/php.conf;

    #access_log /var/log/nginx/wiki-access.log;
    #error_log /var/log/nginx/wiki-error.log info;

#    location ~ /(conf|bin|inc)/ {
#        deny all;
#    }

#    location = /install.php {
#        deny all;
#    }

#    location /data/ {
#        internal;
#    }

    location / { try_files $uri $uri/ @dokuwiki; }

    location @dokuwiki {
        rewrite ^/_media/(.*) /lib/exe/fetch.php?media=$1 last;
        rewrite ^/_detail/(.*) /lib/exe/detail.php?media=$1 last;
        rewrite ^/_export/([^/]+)/(.*) /doku.php?do=export_$1&id=$2 last;
        rewrite ^/(.*) /doku.php?id=$1&$args last;
    }
}
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Configuration
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial Setup
%%%%%%%%%%%%%%%%%%%%

- Go to https://wiki.realmofespionage.xyz/install.php
- Note the hash

%%%%%%%%%%%%%%%%%%%%
%%%%% Hash Fix
%%%%%%%%%%%%%%%%%%%%

sudo -u 'nginx' -e '/var/www/wiki/install.php'

Add new test hash and put comma after last date
-------------------------
$dokuwiki_hash = array(
    '2005-09-22'   => 'e33223e957b0b0a130d0520db08f8fb7',
    '2006-03-05'   => '51295727f79ab9af309a2fd9e0b61acc',
...
    '2015-08-10'   => '263c76af309fbf083867c18a34ff5214',
    'test'         => 'x'
);
-------------------------

- Continue with web setup
- Undo modification afterwards

%%%%%%%%%%%%%%%%%%%%
%%%%% Deny Directives
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/nginx/conf.d/wiki-site.conf' && sudo systemctl reload 'nginx'

%%%%%%%%%%%%%%%%%%%%
%%%%% Post-setup
%%%%%%%%%%%%%%%%%%%%

- Use .htaccess nice URLs option

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Services
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Service Names
%%%%%%%%%%%%%%%%%%%%

wiki-up: DokuWiki updater

%%%%%%%%%%%%%%%%%%%%
%%%%% Timers
%%%%%%%%%%%%%%%%%%%%

wiki-up: daily @ 3:00 AM EST

%%%%%%%%%%%%%%%%%%%%
%%%%% Updater (Service)
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/wiki-up.service'

-------------------------
[Service]
User=nginx
Group=nginx
Type=oneshot
WorkingDirectory=/var/www/wiki
ExecStart='/usr/bin/git' -C '/var/www/wiki' pull origin 'master'
ExecStart='/usr/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Updater (Timer)
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/wiki-up.timer' && sudo systemctl daemon-reload && sudo systemctl enable 'wiki-up.timer' && sudo systemctl start 'wiki-up' 'wiki-up.timer' && sudo systemctl status 'wiki-up' -l

-------------------------
[Unit]
Description=Daily DokuWiki Updater

[Timer]
OnCalendar=*-*-* 03:00:00
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################
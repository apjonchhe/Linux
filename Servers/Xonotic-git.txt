%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Xonotic setup for Realm of Espionage
- Ubuntu Server 16.04

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Software
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo apt-get install git-core build-essential libjpeg9-dev unzip wget

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Firewall Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo ufw allow 26000/udp

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Create User
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo groupadd 'xonotic' && sudo useradd -c 'Xonotic system account' -d '/var/lib/xonotic' -m -r -g 'xonotic' 'xonotic'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Download Initial Source
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cd '/srv' && sudo git clone -b 'master' 'git://git.xonotic.org/xonotic/xonotic.git' '/srv/xonotic' && sudo chown -R 'xonotic':'xonotic' '/srv/xonotic' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Environment Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Switch User
%%%%%%%%%%%%%%%%%%%%

sudo su 'xonotic' -s '/bin/bash'

%%%%%%%%%%%%%%%%%%%%
%%%%% Download Source
%%%%%%%%%%%%%%%%%%%%

cd '/srv/xonotic' && '/srv/xonotic/all' update -l 'best' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Compile
%%%%%%%%%%%%%%%%%%%%

cd '/srv/xonotic' && '/srv/xonotic/all' compile -r 'dedicated' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Configuration
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Copy Template Config
%%%%%%%%%%%%%%%%%%%%

mkdir -p '/var/lib/xonotic/.xonotic/data' && cp '/srv/xonotic/server/server.cfg' '/var/lib/xonotic/.xonotic/data/server.cfg'

%%%%%%%%%%%%%%%%%%%%
%%%%% Edit Config
%%%%%%%%%%%%%%%%%%%%

nano '/var/lib/xonotic/.xonotic/data/server.cfg'

-------------------------
sv_public 1
sv_status_privacy 1
hostname "Realm of Espionage InstaGib ($g_xonoticversion)"
sv_motd "Welcome to the Realm of Espionage Xonotic server! InstaGib all day every day :p" 
maxplayers 16

g_maplist_shuffle 1
g_maplist_mostrecent_count 3

skill 9
minplayers 15
g_maplist_check_waypoints 1

sv_maxidle 60

//sv_weaponstats_file http://www.xonotic.org/weaponbalance/

g_instagib 1

g_grappling_hook 1
g_jetpack 1
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% systemd Scripts
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Notes
%%%%%%%%%%%%%%%%%%%%

- Updater happens daily at 3:00 AM EST
- Restarter happens daily at 3:30 AM EST

%%%%%%%%%%%%%%%%%%%%
%%%%% Xonotic Dedicated Server
%%%%%%%%%%%%%%%%%%%%

sudo -e '/lib/systemd/system/xonotic-dedi.service'

-------------------------
[Unit]
Description=Xonotic Dedicated Server Service

[Service]
User=xonotic
Group=xonotic
Type=simple
WorkingDirectory=/srv/xonotic
ExecStart='/srv/xonotic/all' run 'dedicated'
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Updater
%%%%%%%%%%%%%%%%%%%%

sudo -e '/lib/systemd/system/xonotic-up.service'

-------------------------
[Service]
User=xonotic
Group=xonotic
Type=oneshot
WorkingDirectory=/srv/xonotic
ExecStart='/srv/xonotic/all' clean --reclone
ExecStart='/srv/xonotic/all' compile -r 'dedicated'
ExecStart='/bin/sync'
-------------------------

sudo -e '/lib/systemd/system/xonotic-up.timer'

-------------------------
[Unit]
Description=Daily Xonotic Dedicated Server Updater

[Timer]
OnCalendar=*-*-* 03:00:00
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Restarter
%%%%%%%%%%%%%%%%%%%%

sudo -e '/lib/systemd/system/xonotic-re.service'

-------------------------
[Service]
Type=oneshot
ExecStart='/bin/systemctl' stop 'xonotic-dedi'
ExecStart='/bin/systemctl' stop 'xonotic-dedi'
ExecStart='/bin/sync'
ExecStart='/bin/systemctl' start 'xonotic-dedi'
ExecStart='/bin/systemctl' start 'xonotic-dedi'
-------------------------

sudo -e '/lib/systemd/system/xonotic-re.timer'

-------------------------
[Unit]
Description=Daily Xonotic Restarter

[Timer]
OnCalendar=*-*-* 03:30:00
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Enable and Start Services
%%%%%%%%%%%%%%%%%%%%

sudo systemctl daemon-reload && sudo systemctl enable 'xonotic-dedi' 'xonotic-up.timer' 'xonotic-re.timer' && sudo systemctl start 'xonotic-dedi' 'xonotic-up.timer' 'xonotic-re.timer'

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################
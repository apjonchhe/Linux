%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Boot settings and other info for Raspberry Pi 2 Model B

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% config.txt
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% General
%%%%%%%%%%%%%%%%%%%%

gpu_mem=384

disable_splash=1

force_turbo=1
max_usb_current=1

%%%%%%%%%%%%%%%%%%%%
%%%%% Analog TV (upstairs)
%%%%%%%%%%%%%%%%%%%%

overscan_left=12
overscan_right=12
overscan_top=12
overscan_bottom=12

overscan_scale=1

%%%%%%%%%%%%%%%%%%%%
%%%%% VIZIO LCD (downstairs)
%%%%%%%%%%%%%%%%%%%%

hdmi_force_hotplug=1

config_hdmi_boost=11

%%%%%%%%%%%%%%%%%%%%
%%%%% VIZIO LCD (treadmill/retropie)
%%%%%%%%%%%%%%%%%%%%

hdmi_drive=2

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% cmdline.txt
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% OpenELEC
%%%%% USB Flash Drive as Main
%%%%%%%%%%%%%%%%%%%%

boot=/dev/mmcblk0p1 disk=/dev/sda1 quiet

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Other
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% OpenELEC
%%%%% USB Flash Drive as Main
%%%%%%%%%%%%%%%%%%%%

- Format entire flash drive as GPT with a single ext4 partition

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Ubuntu Server 16.04

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Software
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial
%%%%%%%%%%%%%%%%%%%%

sudo apt-get update && sudo apt-get dist-upgrade -y && sudo apt-get clean && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% General
%%%%%%%%%%%%%%%%%%%%

sudo apt-get install zip unzip aria2 amd64-microcode unar p7zip-full htop

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Firewall
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo ufw allow ssh/tcp && sudo ufw enable

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Network
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Static IP
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/network/interfaces'

-------------------------
#iface enp2s0 inet dhcp 
-------------------------

sudo -e '/etc/network/interfaces.d/static'

-------------------------
iface enp2s0 inet static
address 192.168.1.152
netmask 255.255.255.0
gateway 192.168.1.1
dns-nameservers 192.168.1.158
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% DNS
%%%%%%%%%%%%%%%%%%%%

sudo rm -f '/etc/resolv.conf' && sudo -e '/etc/resolv.conf'

-------------------------
nameserver 192.168.1.158
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% hosts
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% StevenBlack's Amalgamated Hosts
%%%%%%%%%%%%%%%%%%%%

wget 'https://github.com/StevenBlack/hosts/archive/master.zip' -O ~/'master.zip' && unzip ~/'master.zip' && cd ~/'hosts-master' && chmod +x ~/'hosts-master/updateHostsFile.py' && python3 ~/'hosts-master/updateHostsFile.py' && rm -R ~/'hosts-master' ~/'master.zip' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Swappiness
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/sysctl.d/60-swappiness.conf'

-------------------------
vm.swappiness=10
vm.vfs_cache_pressure=50
-------------------------

cat '/proc/sys/vm/swappiness'
cat '/proc/sys/vm/vfs_cache_pressure'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Security
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Prevent non-root from reading dmesg
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/sysctl.d/61-harden.conf'

-------------------------
kernel.dmesg_restrict = 1
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% hdparm Tweaks
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/lib/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak'

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/sbin/hdparm' -A 1 '/dev/sda'
ExecStart='/sbin/hdparm' -B 255 '/dev/sda'
ExecStart='/sbin/hdparm' -J 0 --please-destroy-my-drive '/dev/sda'
ExecStart='/sbin/hdparm' -S 0 '/dev/sda'
ExecStart='/sbin/hdparm' -W 1 '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Automatic Updates
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/lib/systemd/system/ubuntu-up.service'

-------------------------
[Service]
Type=oneshot
ExecStart='/usr/bin/apt-get' update
ExecStart='/usr/bin/apt-get' dist-upgrade -y
ExecStart='/usr/bin/apt-get' autoremove -y
ExecStart='/usr/bin/apt-get' clean
ExecStart='/bin/sync'
-------------------------

sudo -e '/lib/systemd/system/ubuntu-up.timer' && sudo systemctl daemon-reload && sudo systemctl enable 'ubuntu-up.timer' && sudo systemctl start 'ubuntu-up.timer'

-------------------------
[Unit]
Description=Daily sources refresh and dist-upgrade

[Timer]
OnCalendar=*-*-* 05:50:00
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Ubuntu Touch
- Nexus 4

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Install
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Wipe from Recovery
%%%%%%%%%%%%%%%%%%%%

wget 'https://dl.twrp.me/mako/twrp-3.0.2-0-mako.img' -O ~/'Downloads/twrp-mako.img' && sudo fastboot boot ~/'Downloads/twrp-mako.img' && rm ~/'Downloads/twrp-mako.img' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% List Channels
%%%%%%%%%%%%%%%%%%%%

ubuntu-device-flash query --list-channels --device='mako'

%%%%%%%%%%%%%%%%%%%%
%%%%% Install (rc-proposed/ubuntu)
%%%%%%%%%%%%%%%%%%%%

ubuntu-device-flash touch --channel='ubuntu-touch/rc-proposed/ubuntu' --bootstrap

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Software
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% List
%%%%%%%%%%%%%%%%%%%%

click list

%%%%%%%%%%%%%%%%%%%%
%%%%% Remove Scopes
%%%%%%%%%%%%%%%%%%%%

sudo click unregister 'com.ubuntu.developer.webapps.webapp-amazon' --user=phablet && sudo click unregister 'com.ubuntu.developer.webapps.webapp-ebay' --user=phablet && sudo click unregister 'com.ubuntu.developer.webapps.webapp-facebook' --user=phablet && sudo click unregister 'com.ubuntu.developer.webapps.webapp-gmail' --user=phablet && sudo click unregister 'com.ubuntu.developer.webapps.webapp-twitter' --user=phablet && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Files
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mkdir -p '/home/phablet/Ringtones'

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Fedora 23 Workstation

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Download
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Updated Live Images
%%%%%%%%%%%%%%%%%%%%

https://dl.fedoraproject.org/pub/alt/live-respins

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Repositories
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% RPM Fusion
%%%%%%%%%%%%%%%%%%%%

sudo rpm --import 'http://rpmfusion.org/keys?action=AttachFile&do=get&target=RPM-GPG-KEY-rpmfusion-free-fedora-23' && sudo dnf install 'http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-23.noarch.rpm' -y

%%%%%%%%%%%%%%%%%%%%
%%%%% HandBrake + Multimedia
%%%%%%%%%%%%%%%%%%%%

sudo rpm --import 'http://negativo17.org/repos/RPM-GPG-KEY-slaanesh' && sudo dnf config-manager --add-repo='http://negativo17.org/repos/fedora-handbrake.repo'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Software
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial
%%%%%%%%%%%%%%%%%%%%

sudo dnf clean all && sudo dnf update -y && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Remove
%%%%%%%%%%%%%%%%%%%%

sudo dnf remove 'totem'

%%%%%%%%%%%%%%%%%%%%
%%%%% General
%%%%%%%%%%%%%%%%%%%%

sudo dnf install keepass epiphany gnome-calendar htop filezilla perl-Image-ExifTool p7zip unar aria2 gimp transmission wine hdparm nano android-tools

%%%%%%%%%%%%%%%%%%%%
%%%%% RPM Fusion
%%%%%%%%%%%%%%%%%%%%

mpv ffmpeg ffmpegthumbnailer

%%%%%%%%%%%%%%%%%%%%
%%%%% mpv hwdec (RPM Fusion)
%%%%%%%%%%%%%%%%%%%%

mesa-vdpau-drivers
libva-intel-driver

%%%%%%%%%%%%%%%%%%%%
%%%%% Wine (RPM Fusion)
%%%%%%%%%%%%%%%%%%%%

libtxc_dxtn.i686 libtxc_dxtn.x86_64

%%%%%%%%%%%%%%%%%%%%
%%%%% Hammerstorm (RPM Fusion)
%%%%%%%%%%%%%%%%%%%%

HandBrake-gui

%%%%%%%%%%%%%%%%%%%%
%%%%% Hailrake
%%%%%%%%%%%%%%%%%%%%

alsa-tools

%%%%%%%%%%%%%%%%%%%%
%%%%% Google Chrome
%%%%%%%%%%%%%%%%%%%%

sudo rpm --import 'https://dl-ssl.google.com/linux/linux_signing_key.pub' && sudo dnf install 'https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm' -y

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Settings Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Keyboard
%%%%%%%%%%%%%%%%%%%%

gnome-terminal
Ctrl + Alt + T

keepass
Ctrl + Alt + Z

gnome-shell --replace
Ctrl + Alt + \

%%%%%%%%%%%%%%%%%%%%
%%%%% Network
%%%%%%%%%%%%%%%%%%%%

Indoor DNS (IPv4): 192.168.1.158
Indoor DNS (IPv6): 2601:545:4500:91e8:789a:5861:c7b9:8cbf

Outdoor DNS: https://www.opennicproject.org

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Other Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Evolution
%%%%%%%%%%%%%%%%%%%%

TODO: Setup some kind of sync server

%%%%%%%%%%%%%%%%%%%%
%%%%% Files
%%%%%%%%%%%%%%%%%%%%

Sort folders before files and by type
1GB File previews

%%%%%%%%%%%%%%%%%%%%
%%%%% gedit
%%%%%%%%%%%%%%%%%%%%

4 Tab length and use spaces instead of tabs

%%%%%%%%%%%%%%%%%%%%
%%%%% Shotwell (MediaGoblin)
%%%%%%%%%%%%%%%%%%%%

Piwigo
https://media.realmofespionage.xyz/api/piwigo/ws.php

%%%%%%%%%%%%%%%%%%%%
%%%%% Transmission
%%%%%%%%%%%%%%%%%%%%

http://john.bitsurge.net/public/biglist.p2p.gz

%%%%%%%%%%%%%%%%%%%%
%%%%% Terminal
%%%%%%%%%%%%%%%%%%%%

10-notch Transparency

%%%%%%%%%%%%%%%%%%%%
%%%%% mpv (RPM Fusion)
%%%%%%%%%%%%%%%%%%%%

mkdir -p ~/'.config/mpv' && nano ~/'.config/mpv/mpv.conf'

- Change 'vdpau' to 'vaapi' for VA-API
-------------------------
hwdec='vdpau'
vo='opengl-hq'
ao='pulse'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Nano as Default Editor
%%%%%%%%%%%%%%%%%%%%

sudo nano '/etc/sudoers.d/nano'

-------------------------
Defaults editor = /usr/bin/nano
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Epiphany
%%%%%%%%%%%%%%%%%%%%

gsettings set org.gnome.Epiphany keyword-search-url "https://searx.me/?q=%s"
gsettings set org.gnome.Epiphany keyword-search-url "https://startpage.com/do/search?q=%s"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Secure Shell
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Generate SSH Keys
%%%%%%%%%%%%%%%%%%%%

ssh-keygen -o -t 'ed25519'

%%%%%%%%%%%%%%%%%%%%
%%%%% Backup SSH Keys
%%%%%%%%%%%%%%%%%%%%

cd ~ && tar -cvzf ~/'Documents/'$HOSTNAME'-ssh-keys.tar.gz' '.ssh' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Restore SSH Keys
%%%%%%%%%%%%%%%%%%%%

cd ~ && mv ~/'.ssh' ~/'ssh-bak' && tar -xvzf ~/'Documents/'$HOSTNAME'-ssh-keys.tar.gz' '.ssh' && rm ~/'Documents/'$HOSTNAME'-ssh-keys.tar.gz' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Copy SSH PubKey
%%%%%%%%%%%%%%%%%%%%

ssh-copy-id 'espionage724@192.168.1.152'
ssh-copy-id 'pi@192.168.1.158'
ssh-copy-id 'espionage724@192.168.1.163'
ssh-copy-id 'beowulfsdr@192.168.1.166'

%%%%%%%%%%%%%%%%%%%%
%%%%% Force PubKey Auth
%%%%%%%%%%%%%%%%%%%%

- Only do after copying ssh keys from main computers

ssh 'espionage724@192.168.1.152'
ssh 'pi@192.168.1.158'
ssh 'espionage724@192.168.1.163'
ssh 'beowulfsdr@192.168.1.166'

sudo -e '/etc/ssh/sshd_config'

-------------------------
PasswordAuthentication no
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Machine Key Labels
%%%%%%%%%%%%%%%%%%%%

ssh 'espionage724@192.168.1.152'
ssh 'pi@192.168.1.158'
ssh 'espionage724@192.168.1.163'
ssh 'beowulfsdr@192.168.1.166'

nano ~/'.ssh/authorized_keys'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Environment Variables
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% ATI/AMD GPU Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/profile.d/r600-debug.sh'

-------------------------
export R600_DEBUG='sbcl,hyperz,llvm,sisched,forcedma'
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Swappiness
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/sysctl.d/swappiness.conf'

-------------------------
vm.swappiness = 10
vm.vfs_cache_pressure = 50
-------------------------

cat '/proc/sys/vm/swappiness'
cat '/proc/sys/vm/vfs_cache_pressure'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Kernel Hardening
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/sysctl.d/harden.conf'

-------------------------
kernel.dmesg_restrict = 1
kernel.kptr_restrict = 1
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Disable Wayland on GDM
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/gdm/custom.conf'

-------------------------
WaylandEnable=false
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Hammerstorm (desktop)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Notes
%%%%%%%%%%%%%%%%%%%%

- Screen flickering occurs after wake from system suspend due to GPU memory clocks not being locked to max; don't enable auto suspend; TODO: report this

%%%%%%%%%%%%%%%%%%%%
%%%%% Graphics
%%%%%%%%%%%%%%%%%%%%

sudo systemctl mask 'colord' && sudo -e '/etc/X11/xorg.conf.d/graphics.conf'

-------------------------
Section "Device"
    Identifier      "Caribbean Islands"
    Driver          "modesetting"
EndSection

Section "Monitor"
    Identifier      "HDMI-1"
    Gamma           0.8
EndSection

Section "Monitor"
    Identifier      "DVI-D-1"
    Gamma           0.8
EndSection

Section "Monitor"
    Identifier      "DVI-I-1"
    Gamma           0.8
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% GDM Monitor Conf Copy
%%%%%%%%%%%%%%%%%%%%

sudo cp ~/'.config/monitors.xml' ~gdm/'.config/monitors.xml'

%%%%%%%%%%%%%%%%%%%%
%%%%% Boot Options
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/default/grub' && sudo grub2-mkconfig -o '/boot/efi/EFI/fedora/grub.cfg'

-------------------------
iommu=pt usbhid.quirks=0x1B1C:0x1B38:0x20000408
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak' && sudo systemctl status 'hd-tweak' -l

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/usr/sbin/hdparm' -A '1' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -J '0' --please-destroy-my-drive '/dev/sda'
ExecStart='/usr/sbin/hdparm' -S '0' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Spinecrack (laptop)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Graphics
%%%%%%%%%%%%%%%%%%%%

sudo systemctl mask 'colord' && sudo -e '/etc/X11/xorg.conf.d/graphics.conf'

-------------------------
Section "Device"
    Identifier      "Northern Islands"
    Driver          "modesetting"
EndSection

Section "Monitor"
    Identifier      "eDP-1"
    Gamma           0.8
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Boot Options
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/default/grub' && sudo grub2-mkconfig -o '/boot/efi/EFI/fedora/grub.cfg'

-------------------------
usbhid.quirks=0x1B1C:0x1B22:0x20000408
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak' && sudo systemctl status 'hd-tweak' -l

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/usr/sbin/hdparm' -A '1' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -B '255' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -J '0' --please-destroy-my-drive '/dev/sda'
ExecStart='/usr/sbin/hdparm' -S '0' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Hailrake (2-in-1)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Graphics
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/X11/xorg.conf.d/graphics.conf'

-------------------------
Section "Device"
    Identifier      "Sea Islands"
    Driver          "modesetting"
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Audio
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/hp-audio.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hp-audio' && sudo systemctl start 'hp-audio' && sudo systemctl status 'hp-audio' -l

-------------------------
[Unit]
Description=HP Audio Bass and Volume Fix

[Service]
Type=oneshot
ExecStart='/usr/bin/hda-verb' '/dev/snd/hwC1D0' '0x1a' '0x782' '0x61'
ExecStart='/usr/bin/hda-verb' '/dev/snd/hwC1D0' '0x1a' '0x773' '0x2d'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak' && sudo systemctl status 'hd-tweak' -l

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/usr/sbin/hdparm' -A '1' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -B '255' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -S '0' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Piety (mom's laptop)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Graphics
%%%%%%%%%%%%%%%%%%%%

sudo systemctl mask 'colord' && sudo -e '/etc/X11/xorg.conf.d/graphics.conf'

-------------------------
Section "Device"
    Identifier      "Haswell"
    Driver          "modesetting"
EndSection

Section "Monitor"
    Identifier      "eDP-1"
    Gamma           0.8
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak' && sudo systemctl status 'hd-tweak' -l

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/usr/sbin/hdparm' -A '1' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -B '255' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -J '0' --please-destroy-my-drive '/dev/sda'
ExecStart='/usr/sbin/hdparm' -S '0' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Hatebeat (StepMania)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Graphics
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/X11/xorg.conf.d/graphics.conf'

-------------------------
Section "Device"
    Identifier      "R600"
    Driver          "modesetting"
EndSection

Section "Monitor"
    Identifier      "DVI-I-1
    Modeline        "640x480_120.00"  52.41  640 680 744 848  480 481 484 515  -HSync +Vsync
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak' && sudo systemctl status 'hd-tweak' -l

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/usr/sbin/hdparm' -A '1' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -B '255' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -M '0' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -S '0' '/dev/sda'
ExecStart='/usr/sbin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% PulseAudio
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Config
%%%%%%%%%%%%%%%%%%%%

nano ~/'.config/pulse/daemon.conf'

-------------------------
resample-method = soxr-mq
flat-volumes = no
deferred-volume-safety-margin-usec = 1
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% GRUB
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Timeout
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/default/grub'

-------------------------
GRUB_TIMEOUT=3
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% UEFI Update
%%%%%%%%%%%%%%%%%%%%

sudo grub2-mkconfig -o '/boot/efi/EFI/fedora/grub.cfg'

%%%%%%%%%%%%%%%%%%%%
%%%%% BIOS Update
%%%%%%%%%%%%%%%%%%%%

sudo grub2-mkconfig -o '/boot/grub2/grub.cfg'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Automatic Updates
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Service
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/fedora-up.service'

-------------------------
[Service]
Type=oneshot
ExecStart='/usr/bin/dnf' clean 'all'
ExecStart='/usr/bin/dnf' update -y
ExecStart='/usr/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Timer
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/fedora-up.timer' && sudo systemctl daemon-reload && sudo systemctl enable 'fedora-up.timer' && sudo systemctl start 'fedora-up' 'fedora-up.timer' && sudo systemctl status 'fedora-up' -l

-------------------------
[Unit]
Description=Daily repo clean, refresh, and software update

[Timer]
OnCalendar=x
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Times
%%%%%%%%%%%%%%%%%%%%

- Hammerstorm:  05:00:00
- Spinecrack:   05:10:00
- Hailrake:     05:20:00
- Piety:        05:30:00
- Hatebeat:     05:40:00

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Steven Black's Unified Hosts File
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Service
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/hosts-up.service'

-------------------------
[Service]
Type=oneshot
ExecStart='/usr/bin/wget' 'https://github.com/StevenBlack/hosts/archive/master.zip' -O '/tmp/master.zip'
ExecStart='/usr/bin/unzip' '/tmp/master.zip' -d '/tmp'
ExecStart='/usr/bin/python3' '/tmp/hosts-master/updateHostsFile.py' --auto --replace
ExecStart='/usr/bin/rm' -R '/tmp/master.zip' '/tmp/hosts-master'
ExecStart='/usr/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Timer
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/hosts-up.timer' && sudo systemctl daemon-reload && sudo systemctl enable 'hosts-up.timer' && sudo systemctl start 'hosts-up' 'hosts-up.timer' && sudo systemctl status 'hosts-up' -l

-------------------------
[Unit]
Description=Weekly hosts source refresh

[Timer]
OnCalendar=weekly
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Filesystem Defragmentation
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Service
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/fs-m.service'

- Change 'hammerstorm' to lowercase hostname
- TODO: Verify on BIOS machines with df -hT
-------------------------
[Service]
Type=oneshot
ExecStart='/usr/sbin/e4defrag' '/dev/sda2'
ExecStart='/usr/sbin/e4defrag' '/dev/mapper/fedora_hammerstorm-home'
ExecStart='/usr/sbin/e4defrag' '/dev/mapper/fedora_hammerstorm-root'
ExecStart='/usr/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Timer
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/fs-m.timer' && sudo systemctl daemon-reload && sudo systemctl enable 'fs-m.timer' && sudo systemctl start 'fs-m' 'fs-m.timer' && sudo systemctl status 'fs-m' -l

-------------------------
[Unit]
Description=Weekly Filesystem Defrag

[Timer]
OnCalendar=weekly
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Firefox Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% about:config
%%%%%%%%%%%%%%%%%%%%

cd ~/'.mozilla/firefox/'*'.default' && nano 'user.js' && cd ~

-------------------------
user_pref("media.mp3.enabled", true);
user_pref("layout.frame_rate.precise", true);
user_pref("mousewheel.min_line_scroll_amount", 40);
user_pref("javascript.options.mem.high_water_mark", 32);
user_pref("javascript.options.mem.max", 51200);
user_pref("dom.storage.enabled", true);
user_pref("dom.event.clipboardevents.enabled", true);
user_pref("media.fragmented-mp4.exposed", true);
user_pref("media.fragmented-mp4.ffmpeg.enabled", true);
user_pref("media.mediasource.ignore_codecs", true);
user_pref("layers.acceleration.force-enabled", true);
user_pref("layers.offmainthreadcomposition.enabled", true);
user_pref("extensions.jid1-BoFifL9Vbdl2zQ@jetpack.showReleaseNotes", false);
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Extenstions (7)
%%%%%%%%%%%%%%%%%%%%

xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/privacy-settings/versions' && xdg-open 'https://www.eff.org/privacybadger' && xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/versions' && xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/gnotifier/versions' && xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/versions' && xdg-open 'https://www.eff.org/https-everywhere' && xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/disable-hello-pocket-reader/versions'

%%%%%%%%%%%%%%%%%%%%
%%%%% Extensions Configuration
%%%%%%%%%%%%%%%%%%%%

- uBlock filters (disable EasyList, disable EasyPrivacy, enable both Adblock killers, enable Easy+Fanboy Ultimate)
- Full privacy with Privacy Settings button

%%%%%%%%%%%%%%%%%%%%
%%%%% Search Engines
%%%%%%%%%%%%%%%%%%%%

https://searx.me/about#
https://startpage.com/eng/download-startpage-plugin.html?hmb=1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Noteable Folders and Commands
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Shortcuts
%%%%%%%%%%%%%%%%%%%%

/usr/share/applications
~/.local/share/applications

%%%%%%%%%%%%%%%%%%%%
%%%%% Icons
%%%%%%%%%%%%%%%%%%%%

/usr/share/icons/hicolor
~/.local/share/icons/hicolor

%%%%%%%%%%%%%%%%%%%%
%%%%% yum/dnf Repos
%%%%%%%%%%%%%%%%%%%%

/etc/yum.repos.d

%%%%%%%%%%%%%%%%%%%%
%%%%% Check CPU Frequency
%%%%%%%%%%%%%%%%%%%%

grep 'MHz' '/proc/cpuinfo'
watch -n 0.1 grep \'cpu MHz\' '/proc/cpuinfo'

%%%%%%%%%%%%%%%%%%%%
%%%%% Filesystem Info
%%%%%%%%%%%%%%%%%%%%

df -hT

%%%%%%%%%%%%%%%%%%%%
%%%%% Hard Drive Wipe
%%%%%%%%%%%%%%%%%%%%

- Make sure to change sda if not wanting to do sda

sudo hdparm -I '/dev/sda' | grep 'not'
sudo hdparm --user-master u --security-set-pass 'x' '/dev/sda'
sudo hdparm --user-master u --security-erase-enhanced 'x' '/dev/sda'

%%%%%%%%%%%%%%%%%%%%
%%%%% Get GCC Compile Flags
%%%%%%%%%%%%%%%%%%%%

gcc -v -E -x c -march=native -mtune=native - < /dev/null 2>&1 | grep cc1 | perl -pe 's/ -mno-\S+//g; s/^.* - //g;'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Privacy
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Remove metadata
%%%%%%%%%%%%%%%%%%%%

exiftool -all= *.* -overwrite_original

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################